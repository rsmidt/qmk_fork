#include "ergodox_ez.h"
#include "debug.h"
#include "action_layer.h"
#include "version.h"
#include "keymap_german.h"
#include "keymap_nordic.h"

#define GERMAN_CHAR(keycode, small, large) case keycode: \
                                              if (record->event.pressed) { \
                                                key_timer = timer_read(); \
                                              } else { \
                                                if (timer_elapsed(key_timer) > 150) { \
                                                  send_unicode_hex_string(large); \
                                                } else { \
                                                  send_unicode_hex_string(small); \
                                                } \
                                              } \
                                              ret = false; \
                                              break; \

typedef union {
    uint32_t raw;
    struct {
        bool is_darwin :1;
    };
} user_config_t;

user_config_t user_config;

enum custom_keycodes {
    PLACEHOLDER = SAFE_RANGE, // can always be here
    EPRM,
    RGB_SLD,
    UC_GERMAN_UE,
    UC_GERMAN_AE,
    UC_GERMAN_OE,
    UC_GERMAN_SS,
    TOGGLE_DARWIN,
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
  [0] = LAYOUT_ergodox(
  KC_EQUAL,       KC_1,                   KC_2,             KC_3,             KC_4,          KC_5,                   KC_LEFT,
  KC_DELETE,      LGUI_T(KC_Q),           KC_W,             KC_E,             KC_R,          KC_T,                   TG(1),
  LT(1,KC_ESCAPE),KC_A,                   KC_S,             KC_D,             KC_F,          KC_G,
  KC_LSHIFT,      CTL_T(KC_Z),            KC_X,             KC_C,             KC_V,          KC_B,                   KC_HYPR,
  LT(1,KC_GRAVE), KC_QUOTE,               LALT(KC_LSHIFT),  KC_TRANSPARENT,   MO(3),
                                                                                             ALT_T(KC_APPLICATION),  KC_LGUI,
                                                                                                                     KC_HOME,
                                                                              KC_SPACE,      KC_BSPACE,              KC_END,

  KC_RIGHT,       KC_6,              KC_7,         KC_8,             KC_9,                   KC_0,              KC_MINUS,
  TG(1),          KC_Y,              KC_U,         KC_I,             KC_O,                   RGUI_T(KC_P),      KC_BSLASH,
                  KC_H,              KC_J,         KC_K,             KC_L,                   LT(2,KC_SCOLON),   KC_QUOTE,
  KC_MEH,         KC_N,              KC_M,         KC_COMMA,         KC_DOT,                 RCTL_T(KC_SLASH),  KC_RSHIFT,
  KC_LEAD,        KC_DOWN,           KC_LBRACKET,  KC_RBRACKET,      MO(1),
  KC_RALT,        CTL_T(KC_ESCAPE),
  KC_PGUP,
  KC_PGDOWN,      KC_TAB,            KC_ENTER
  ),

  [1] = LAYOUT_ergodox(
  KC_ESCAPE,       KC_F1,           KC_F2,           KC_F3,           KC_F4,           KC_F5,     KC_TRANSPARENT,
  KC_TRANSPARENT,  KC_EXLM,         KC_AT,           KC_LCBR,         KC_RCBR,         KC_PIPE,   KC_TRANSPARENT,
  KC_TRANSPARENT,  UC_GERMAN_AE,    UC_GERMAN_SS,    KC_LPRN,         KC_RPRN,         KC_GRAVE,
  KC_TRANSPARENT,  KC_PERC,         KC_CIRC,         KC_LBRACKET,     KC_RBRACKET,     KC_TILD,   KC_TRANSPARENT,
  KC_TRANSPARENT,  KC_TRANSPARENT,  KC_TRANSPARENT,  KC_TRANSPARENT,  KC_TRANSPARENT,
                                                                                 KC_TRANSPARENT,  KC_TRANSPARENT,
                                                                                                  KC_TRANSPARENT,
                                                                KC_TRANSPARENT,  KC_TRANSPARENT,  KC_TRANSPARENT,

  KC_TRANSPARENT,  KC_F6,           KC_F7,           KC_F8,           KC_F9,           KC_F10,          KC_F11,
  KC_TRANSPARENT,  KC_TRANSPARENT,  UC_GERMAN_UE,    KC_INSERT,       UC_GERMAN_OE,    KC_ASTR,         KC_F12,
                   KC_LEFT,         KC_DOWN,         KC_UP,           KC_RIGHT,        KC_TRANSPARENT,  KC_TRANSPARENT,
  KC_TRANSPARENT,  KC_TRANSPARENT,  UC(0x2642),      KC_TRANSPARENT,  KC_TRANSPARENT,  KC_BSLASH,       KC_TRANSPARENT,
  KC_TRANSPARENT,  KC_DOT,          KC_TRANSPARENT,  KC_EQUAL,        KC_TRANSPARENT,
  KC_TRANSPARENT,  KC_TRANSPARENT,
  KC_TRANSPARENT,
  UC_M_LN,         KC_TRANSPARENT,  KC_TRANSPARENT
  ),

  #define NEXT_PROJECT_WINDOW LALT(LGUI(KC_GRAVE))
  #define PREV_PROJECT_WINDOW LALT(LGUI(LSFT(KC_GRAVE)))

  [2] = LAYOUT_ergodox(
  KC_TRANSPARENT,  KC_TRANSPARENT,  KC_MPRV,         KC_MPLY,            KC_MNXT,          KC_TRANSPARENT,  KC_TRANSPARENT,
  KC_TRANSPARENT,  KC_TRANSPARENT,  KC_TRANSPARENT,  KC_MS_UP,           KC_TRANSPARENT,   KC_TRANSPARENT,  KC_TRANSPARENT,
  KC_TRANSPARENT,  KC_TRANSPARENT,  KC_MS_LEFT,      KC_MS_DOWN,         KC_MS_RIGHT,      KC_TRANSPARENT,
  KC_TRANSPARENT,  KC_TRANSPARENT,  KC_AUDIO_MUTE,   KC_AUDIO_VOL_DOWN,  KC_AUDIO_VOL_UP,  KC_TRANSPARENT,  KC_TRANSPARENT,
  KC_TRANSPARENT,  KC_TRANSPARENT,  KC_TRANSPARENT,  KC_MS_BTN1,         KC_MS_BTN2,
                                                                                           KC_TRANSPARENT,  KC_TRANSPARENT,
                                                                                                            KC_TRANSPARENT,
                                                                          KC_TRANSPARENT,  KC_TRANSPARENT,  TOGGLE_DARWIN,

  KC_TRANSPARENT,  KC_TRANSPARENT,  KC_TRANSPARENT,  KC_TRANSPARENT,  KC_TRANSPARENT,  KC_TRANSPARENT,       KC_TRANSPARENT,
  KC_TRANSPARENT,  KC_TRANSPARENT,  KC_KP_7,         KC_KP_8,         KC_KP_9,         KC_KP_ASTERISK,       KC_TRANSPARENT,
                   KC_TRANSPARENT,  KC_KP_4,         KC_KP_5,         KC_6,            KC_TRANSPARENT,       KC_MEDIA_PLAY_PAUSE,
  KC_TRANSPARENT,  KC_TRANSPARENT,  KC_KP_1,         KC_KP_2,         KC_3,            KC_TRANSPARENT,       KC_TRANSPARENT,
  KC_TRANSPARENT,  KC_TRANSPARENT,  KC_KP_0,         KC_TRANSPARENT,  KC_TRANSPARENT,
  KC_TRANSPARENT,  KC_TRANSPARENT,
  KC_TRANSPARENT,
  RESET,           KC_TRANSPARENT,  KC_WWW_BACK
  ),

  [3] = LAYOUT_ergodox(
  KC_TRANSPARENT,  KC_TRANSPARENT,  KC_TRANSPARENT,  KC_TRANSPARENT,     KC_TRANSPARENT,   KC_TRANSPARENT,  KC_TRANSPARENT,
  KC_TRANSPARENT,  KC_TRANSPARENT,  KC_TRANSPARENT,  KC_MS_UP,           KC_TRANSPARENT,   KC_TRANSPARENT,  KC_TRANSPARENT,
  KC_TRANSPARENT,  KC_TRANSPARENT,  KC_MS_LEFT,      KC_MS_DOWN,         KC_MS_RIGHT,      KC_TRANSPARENT,
  KC_TRANSPARENT,  KC_TRANSPARENT,  KC_AUDIO_MUTE,   KC_AUDIO_VOL_DOWN,  KC_AUDIO_VOL_UP,  KC_TRANSPARENT,  KC_TRANSPARENT,
  KC_TRANSPARENT,  KC_TRANSPARENT,  KC_TRANSPARENT,  KC_MS_BTN1,         KC_MS_BTN2,
                                                                                           KC_TRANSPARENT,  KC_TRANSPARENT,
                                                                                                            KC_TRANSPARENT,
                                                                          KC_TRANSPARENT,  KC_TRANSPARENT,  KC_TRANSPARENT,

  KC_TRANSPARENT,  KC_TRANSPARENT,  KC_TRANSPARENT,        KC_TRANSPARENT,      KC_TRANSPARENT,  KC_TRANSPARENT,       KC_TRANSPARENT,
  KC_TRANSPARENT,  KC_TRANSPARENT,  LCTL(KC_DOWN),         LCTL(KC_UP),         KC_TRANSPARENT,  KC_KP_ASTERISK,       KC_TRANSPARENT,
                   S(KC_TAB),       S(KC_GRAVE),           KC_GRAVE,            KC_TAB,          KC_TRANSPARENT,       KC_MEDIA_PLAY_PAUSE,
  KC_TRANSPARENT,  KC_TRANSPARENT,  PREV_PROJECT_WINDOW,   NEXT_PROJECT_WINDOW, KC_3,            KC_TRANSPARENT,       KC_TRANSPARENT,
  KC_TRANSPARENT,  KC_TRANSPARENT,  KC_KP_0,               KC_TRANSPARENT,      KC_TRANSPARENT,
  KC_TRANSPARENT,  KC_TRANSPARENT,
  KC_TRANSPARENT,
  KC_TRANSPARENT,  KC_TRANSPARENT,  KC_WWW_BACK
  ),
};

// Read the custom eeprom settings.
void keyboard_post_init_user(void) {
    user_config.raw = eeconfig_read_user();
}

// Set useful defaults in case of eeprom reset.
void eeconfig_init_user(void) {
    user_config.raw = 0;
    user_config.is_darwin = false;

    eeconfig_update_user(user_config.raw);
}

static uint16_t key_timer;

bool handle_unicodes(uint16_t keycode, keyrecord_t *record) {
    bool ret = true;

    switch (keycode) {
        GERMAN_CHAR(UC_GERMAN_UE, "00FC", "00DC")
        GERMAN_CHAR(UC_GERMAN_AE, "00E4", "00C4")
        GERMAN_CHAR(UC_GERMAN_OE, "00F6", "00D6")
        GERMAN_CHAR(UC_GERMAN_SS, "00DF", "1E9E")
    }

    return ret;
}

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
  bool ret = true;

  ret = handle_unicodes(keycode, record);

  switch (keycode) {
    // dynamically generate these.
    case EPRM:
        if (record->event.pressed) {
            eeconfig_init();
        }

        ret = false;

    // toggle darwin mode
    case TOGGLE_DARWIN:
        if (record->event.pressed) {
            user_config.is_darwin ^= 1;

            eeconfig_update_user(user_config.raw);
        }
  }

  return ret;
}

// is alt currently held
bool is_alt_active = false;

uint32_t layer_state_set_user(uint32_t state) {
    uint8_t layer = biton32(state);

    // layer 3 acts as a alt + tab layer
    if (layer == 3) {
        if (user_config.is_darwin) {
            SEND_STRING(SS_DOWN(X_LGUI));
        } else {
            SEND_STRING(SS_DOWN(X_LALT));
        }

        is_alt_active = true;
    } else if (is_alt_active) {
        if (user_config.is_darwin) {
            SEND_STRING(SS_UP(X_LGUI));
        } else {
            SEND_STRING(SS_UP(X_LALT));
        }

        is_alt_active = false;
    }

    return state;
};

LEADER_EXTERNS();

void matrix_scan_user(void) {
    LEADER_DICTIONARY() {
        leading = false;
        leader_end();

        // VIM: Save
        SEQ_ONE_KEY(KC_W) {
            SEND_STRING(":wa"SS_TAP(X_ENTER));
        }

        // IntelliJ: Debug/Restart
        // Has to be remapped in intellij if on darwin
        SEQ_ONE_KEY(KC_D) {
            SEND_STRING(SS_LSFT(SS_TAP(X_F9)));
        }

        // IntelliJ: Reformat Code
        SEQ_ONE_KEY(KC_F) {
            if (user_config.is_darwin) {
                SEND_STRING(SS_LCTRL(SS_LGUI("l")));
            } else {
                SEND_STRING(SS_LCTRL(SS_LALT("l")));
            }
        }

        // IntelliJ: Search in Path
        SEQ_TWO_KEYS(KC_S, KC_G) {
            if (user_config.is_darwin) {
                SEND_STRING(SS_LGUI(SS_LSFT("f")));
            } else {
                SEND_STRING(SS_LCTRL(SS_LSFT("f")));
            }
        }

        // IntelliJ: Search Classes
        SEQ_TWO_KEYS(KC_S, KC_C) {
            if (user_config.is_darwin) {
                SEND_STRING(SS_LGUI("o"));
            } else {
                SEND_STRING(SS_LCTRL("n"));
            }
        }

        // IntelliJ: Search Files
        SEQ_TWO_KEYS(KC_S, KC_F) {
            if (user_config.is_darwin) {
                SEND_STRING(SS_LGUI(SS_LSFT("o")));
            } else {
                SEND_STRING(SS_LCTRL(SS_LSFT("n")));
            }
        }

        // IntelliJ: Search Symbols
        SEQ_TWO_KEYS(KC_S, KC_S) {
            if (user_config.is_darwin) {
                SEND_STRING(SS_LALT(SS_LGUI("o")));
            } else {
                SEND_STRING(SS_LCTRL(SS_LSFT(SS_LALT("n"))));
            }
        }

        // IntelliJ: Search Actions
        SEQ_TWO_KEYS(KC_S, KC_A) {
            if (user_config.is_darwin) {
                SEND_STRING(SS_LGUI(SS_LSFT("a")));
            } else {
                SEND_STRING(SS_LCTRL(SS_LSFT("a")));
            }
        }

        // IntelliJ: Search Symbols in File
        SEQ_THREE_KEYS(KC_S, KC_S, KC_F) {
            if (user_config.is_darwin) {
                SEND_STRING(SS_LGUI(SS_TAP(X_F12)));
            } else {
                SEND_STRING(SS_LCTRL(SS_TAP(X_F12)));
            }
        }

        // IntelliJ: Open Settings
        SEQ_TWO_KEYS(KC_I, KC_S) {
            if (user_config.is_darwin) {
                SEND_STRING(SS_LGUI(","));
            } else {
                SEND_STRING(SS_LCTRL(SS_LALT("s")));
            }
        }

        // GNOME: New Terminal
        SEQ_TWO_KEYS(KC_T, KC_N) {
            SEND_STRING(SS_LCTRL(SS_LALT(SS_LSFT("t"))));
        }

        // IntelliJ: Refactor Menu
        SEQ_TWO_KEYS(KC_R, KC_A) {
            // We use LGUI in addition so we do not clutch with 'GNOME: New Terminal'
            SEND_STRING(SS_LCTRL(SS_LALT(SS_LSFT(SS_LGUI("t")))));
        }

        // IntelliJ: Refactor Rename
        SEQ_TWO_KEYS(KC_R, KC_R) {
            SEND_STRING(SS_LSFT(SS_TAP(X_F6)));
        }

        // IntelliJ: Refactor Extract Method
        SEQ_THREE_KEYS(KC_R, KC_E, KC_M) {
            if (user_config.is_darwin) {
                SEND_STRING(SS_LGUI(SS_LALT("m")));
            } else {
                SEND_STRING(SS_LCTRL(SS_LALT("m")));
            }
        }

        // IntelliJ: Refactor Extract Variable
        SEQ_THREE_KEYS(KC_R, KC_E, KC_V) {
            if (user_config.is_darwin) {
                SEND_STRING(SS_LGUI(SS_LALT("v")));
            } else {
                SEND_STRING(SS_LCTRL(SS_LALT("v")));
            }
        }

        // IntelliJ: Add all missing imports
        SEQ_TWO_KEYS(KC_I, KC_A) {
            SEND_STRING(SS_LALT(SS_LSFT(SS_TAP(X_ENTER))));
        }

        // OS: Lock system
        SEQ_TWO_KEYS(KC_S, KC_L) {
            if (user_config.is_darwin) {
                SEND_STRING(SS_LGUI(SS_LCTRL("q")));
            } else {
                SEND_STRING(SS_LGUI("L"));
            }
        }

        // OS: Application lauchner
        SEQ_ONE_KEY(KC_A) {
            if (user_config.is_darwin) {
                SEND_STRING(SS_LGUI(SS_TAP(X_SPACE)));
            } else {
                SEND_STRING(SS_TAP(X_LGUI));
            }
        }
    }
}
